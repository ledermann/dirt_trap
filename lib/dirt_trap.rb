require 'dirt_trap/version'

ActiveRecord::Base.class_eval do
  def self.has_dirt_trap(options={})
    if include?(DirtTrap::InstanceMethods)
      raise ArgumentError, 'DirtTrap can not be used twice in the same class!'
    end

    include DirtTrap::InstanceMethods
    extend DirtTrap::ClassMethods

    unless options[:for].is_a?(Symbol)
      raise ArgumentError, 'Option :for missing or not a symbol!'
    end

    options[:to] ||= "#{options[:for]}_changes"

    class_attribute :dt_options
    self.dt_options = options

    unless dt_association
      raise ArgumentError, 'Option :for is not a belongs_to association!'
    end

    accepts_nested_attributes_for options[:for], :update_only => true, :allow_destroy => false

    serialize dt_changes_attribute, coder: YAML
    attr_accessor dt_accessor_name

    before_validation :dt_copy_changes_to_master
    before_destroy :dt_revert_master!
    before_update :dt_revert_previous_master!
  end
end

module DirtTrap
  module ClassMethods
    def dt_changes_attribute
      dt_options[:to]
    end

    def dt_accessor_name
      "#{dt_options[:for]}_attributes"
    end

    def dt_association
      reflect_on_all_associations(:belongs_to).find { |assoc| assoc.name == dt_options[:for] }
    end

    def dt_master_class
      dt_association.class_name.constantize
    end

    def dt_master_id_key_name
      dt_association.foreign_key
    end
  end

  module InstanceMethods
    def dt_changes
      send(self.class.dt_changes_attribute)
    end

    def dt_changes=(value)
      send("#{self.class.dt_changes_attribute}=", value)
    end

    def dt_changed_value?(attribute)
      (dt_changes||{})[attribute].present?
    end

    def dt_new_value(attribute)
      result = (dt_changes||{})[attribute]
      result.is_a?(Array) ? result.second : nil
    end

    def dt_update_master_changes(hash)
      return unless hash.present?

      self.dt_changes ||= {}
      hash.each_pair do |k,v|
        if dt_changes[k] && dt_changes[k].reverse == v
          # revert previous change
          dt_changes.delete(k)
        elsif dt_changes[k]
          # update an existing change
          dt_changes[k][1] = v[1]
        else
          # add a new change
          dt_changes[k] = v
        end
      end
    end

    def dt_revert_master!
      if dt_master && dt_changes.present?
        if dt_revert?(dt_master)
          dt_revert!(dt_master)
          self.dt_changes = nil
        end
      end
      true
    end

  private
    def dt_master
      send(dt_options[:for])
    end

    def dt_master_attributes
      send(self.class.dt_accessor_name) || {}
    end

    def dt_master_id_was
      send("#{self.class.dt_master_id_key_name}_was")
    end

    def dt_master_id_changed?
      send("#{self.class.dt_master_id_key_name}_changed?")
    end

    def dt_revert?(master)
      if dt_options[:revert_if]
        dt_options[:revert_if].call(master, self)
      else
        true
      end
    end

    def dt_master_save!(master)
      # May be overriden to add permission checks
      master.save!
    end

    def dt_revert!(master)
      return if dt_changes.blank?

      dt_changes.each_pair do |k,v|
        master.send("#{k}=", v.first)
      end

      dt_master_save!(master)
    end

    def dt_revert_previous_master!
      if dt_master_id_changed? && dt_master_id_was && dt_changes.present?
        if previous_master = self.class.dt_master_class.find(dt_master_id_was)
          if dt_revert?(previous_master)
            dt_revert!(previous_master)
            self.dt_changes = nil
          end
        end
      end
      true
    end

    def dt_copy_changes_to_master
      if dt_master
        dt_master.attributes = dt_master_attributes
        send(dt_options[:before_validation]) if dt_options[:before_validation].is_a?(Symbol)
        dt_update_master_changes(dt_master.changes)
      end
    end
  end
end
