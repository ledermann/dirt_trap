# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'dirt_trap/version'

Gem::Specification.new do |gem|
  gem.name          = "dirt_trap"
  gem.version       = DirtTrap::VERSION
  gem.authors       = ["Georg Ledermann"]
  gem.email         = ["mail@georg-ledermann.de"]
  gem.description   = %q{ActiveRecord extension to track changes of attributes}
  gem.summary       = %q{No summary yet}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.add_dependency 'activerecord'

  gem.add_development_dependency 'rspec'
  gem.add_development_dependency 'sqlite3'
  gem.add_development_dependency 'rake'
end
