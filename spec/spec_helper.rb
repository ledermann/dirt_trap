require 'active_record'
require 'dirt_trap'

I18n.enforce_available_locales = false

ActiveRecord::Base.establish_connection(:adapter => "sqlite3", :database => ":memory:")

ActiveRecord::Schema.verbose = false
ActiveRecord::Schema.define(:version => 1) do
  create_table :journals do |t|
    t.integer :master_id
    t.text :master_changes
  end

  create_table :masters do |t|
    t.string :name
    t.integer :value
  end
end

class Journal < ActiveRecord::Base
  belongs_to :master
  has_dirt_trap :for => :master,
                :before_validation => :before_validation_callback,
                :revert_if => lambda { |master, journal| master.journals.last == journal }

  def before_validation_callback
  end
end

class Master < ActiveRecord::Base
  has_many :journals, -> { order 'id ASC' }

  validates_presence_of :name
end
