require 'spec_helper'

describe Journal, 'class methods' do
  it 'should know the changes attribute name' do
    expect(Journal.dt_changes_attribute).to eq('master_changes')
  end

  it 'should know the accessor name' do
    expect(Journal.dt_accessor_name).to eq('master_attributes')
  end

  it 'should know the master association' do
    expect(Journal.dt_association.name).to eq(:master)
  end

  it 'should know master class' do
    expect(Journal.dt_master_class).to eq(Master)
  end

  it 'should know the key name' do
    expect(Journal.dt_master_id_key_name).to eq('master_id')
  end
end

describe Journal, 'instance methods' do
  it 'should access the master' do
    expect(Journal.new).to respond_to(:master_attributes)
    expect(Journal.new).to respond_to(:master_attributes=)
  end

  it 'should have dt_new_value' do
    journal = Journal.new :dt_changes => { 'value' => [ nil, 42 ]}

    expect(journal.dt_changed_value?('value')).to be_truthy
    expect(journal.dt_changed_value?('name')).to be_falsey

    expect(journal.dt_new_value('value')).to eq(42)
    expect(journal.dt_new_value('foo')).to eq(nil)
  end

  describe 'dt_update_master_changes' do
    it 'should add key' do
      journal = Journal.new
      journal.dt_update_master_changes({ 'name' => [ 'Foo', 'Bar']})
      expect(journal.master_changes).to eq({ 'name' => [ 'Foo', 'Bar']})
    end

    it 'should update key' do
      journal = Journal.new :master_changes => { 'name' => [ 'Foo', 'Bar']}
      journal.dt_update_master_changes({ 'name' => [ '123', 'Baz']})
      expect(journal.master_changes).to eq({ 'name' => [ 'Foo', 'Baz']})
    end

    it 'should delete key' do
      journal = Journal.new :master_changes => { 'name' => [ 'Foo', 'Bar']}
      journal.dt_update_master_changes({ 'name' => [ 'Bar', 'Foo']})
      expect(journal.master_changes).to eq({})
    end
  end
end

describe Journal, 'callbacks' do
  before :each do
    @master = Master.create! :name => 'Foo'
  end

  it 'should call callback on validation' do
    journal = @master.journals.build
    expect(journal).to receive(:before_validation_callback)
    journal.valid?
  end
end

describe Journal, 'updating the master' do
  before :each do
    @master = Master.create! :name => 'Foo'
  end

  context 'with invalid data' do
    before :each do
      @journal1 = @master.journals.build :master_attributes => { :name => nil }
    end

    it 'should fail' do
      expect(@journal1).not_to be_valid
    end
  end

  context 'with valid data' do
    before :each do
      @journal1 = @master.journals.create! :master_attributes => { :name => 'Bar1' }
    end

    it 'should change master' do
      expect(@journal1.master_changes).to eq({ 'name' => [ 'Foo', 'Bar1' ]})
      expect(@master.reload.name).to eq('Bar1')
    end

    it 'should revert the change explicit' do
      @journal1.dt_revert_master!
      expect(@master.reload.name).to eq('Foo')
    end

    describe 'changing to another master' do
      before :each do
        @master2 = Master.create! :name => 'Foo2'
        @journal1.update :master => @master2
      end

      it 'should revert the previous master' do
        expect(@master.reload.name).to eq('Foo')
      end

      it 'should empty the changes' do
        expect(@journal1.master_changes).to be_nil
      end
    end

    describe 'on destroying' do
      before :each do
        @journal2 = @master.journals.create! :master_attributes => { :name => 'Bar2' }
      end

      it 'should revert the change for the latest record' do
        @journal2.destroy
        expect(@master.reload.name).to eq('Bar1')
      end

      it 'should revert the change for all records' do
        @journal2.destroy
        @journal1.destroy
        expect(@master.reload.name).to eq('Foo')
      end

      it 'should not revert the change on destroying an old record' do
        @journal1.destroy
        expect(@master.reload.name).to eq('Bar2')
      end
    end
  end
end
